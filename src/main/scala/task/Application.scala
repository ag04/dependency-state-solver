package task

import task.exception.{ParsingException, ServiceViolationException}
import task.parser.{Parser, ParserImpl}
import task.service.{EventService, EventServiceImpl}

/**
  * Entry point to application.
  */
object Application {

  var eventService: EventService = new EventServiceImpl
  var parser: Parser = new ParserImpl

  /**
    * Entry function to application.
    */
  def main(args: Array[String]): Unit = {

    if (args.length != 2) {
      println("Exactly two arguments expected!")
      return
    }

    try {
      var graph = parser.parseGraph(args(0))
      val events = parser.parseEvents(args(1))

      eventService.sortEventsByTimestamps(events).foreach(x => {
        graph = eventService.applyEventOnGraph(x, graph)
      })

      println(graph.toJsonString(0))
    } catch {
      case iae: ParsingException => println(s"Input files not in defined structure: ${iae.getMessage}")
      case se: ServiceViolationException => println(s"Service violation: ${se.getMessage}")
      case nse: NoSuchElementException => println(s"No such StateType: ${nse.getMessage}")
    }
  }

}
