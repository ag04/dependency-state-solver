package task.service

import task.model.{Event, Graph}

trait EventService {

  /**
    * Sorts events by timestamps ascending.
    *
    * @param events events
    * @return sorted events
    */
  def sortEventsByTimestamps(events: List[Event]): List[Event]

  /**
    * Applies given event and creates new graph,
    * given that graph was initially in proper state
    *
    * @param event event to apply
    * @return graph after applied event
    */
  def applyEventOnGraph(event: Event, graph: Graph): Graph


}
