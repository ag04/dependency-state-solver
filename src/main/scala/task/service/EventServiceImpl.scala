package task.service

import task.exception.ServiceViolationException
import task.model.StateType.StateType
import task.model.{Component, Event, Graph, StateType}

class EventServiceImpl extends EventService {

  /**
    * Sorts events by timestamps ascending.
    *
    * @param events events
    * @return sorted events
    */
  def sortEventsByTimestamps(events: List[Event]): List[Event] = {
    events.sortWith(_.timestamp < _.timestamp)
  }

  /**
    * Applies given event and creates new graph,
    * given that graph was initially in proper state
    *
    * @param event event to apply
    * @return graph after applied event
    */
  def applyEventOnGraph(event: Event, graph: Graph): Graph = {
    val components = graph.components
    val component = components.find(_.id == event.componentId).orElse(
      throw ServiceViolationException("Unknown component in event list: " + event.componentId)).get

    if (component.checkStates.getOrElse(event.checkState, null) == null)
      throw ServiceViolationException("Unknown check state: " + event.checkState + ", for component: " + event.componentId)

    val checkStates = component.checkStates + (event.checkState -> event.stateType)
    val ownState = checkStates.values.max

    val updatedComponent = new Component(component.id, ownState, calcDerivedState(component, ownState, graph.components),
      checkStates, component.dependsOnNames, component.dependencyOfNames)
    updatedComponent.initDependencies(components)

    val updatedComponents = graph.components.filter(_.id != updatedComponent.id) ::: List(updatedComponent)

    def newComponents = go(List(updatedComponent), ownState, List())

    //recursion for "updating" components dependent on changed component(s)
    @annotation.tailrec
    def go(changedComponents: List[Component], ownState: StateType, newComponents: List[Component]): List[Component] = {
      if (changedComponents.isEmpty) newComponents
      else go(changedComponents.flatMap(_.dependencyOf).filter(!newComponents.contains(_)), ownState,
        newComponents ::: changedComponents.map(a => new Component(a.id, a.ownState, calcDerivedState(a, ownState, updatedComponents),
          a.checkStates, a.dependsOnNames, a.dependencyOfNames)))
    }

    //add missing components
    val list = components.filter(x => !newComponents.map(y => y.id).contains(x.id))
    val newGraphComponents = newComponents ::: list

    //sort as original components
    val ids = components.map(_.id)
    val newGraphMap = (newGraphComponents map (_.id) zip newGraphComponents).toMap
    val newGraph = ids.map(newGraphMap)

    new Graph(newGraph)
  }

  /**
    * Calculates derived state as described in task,
    * if max state of derived and own state is clear, return no_data.
    *
    * @param c        component
    * @param ownState calculated own state of changed component
    * @return derived state as defined in task
    */
  private def calcDerivedState(c: Component, ownState: StateType, components: List[Component]) = {
    def maxState = (components.filter(x => c.dependsOnNames.contains(x.id)).map(x => x.ownState) ::: List(ownState)).max

    if (maxState == StateType.clear) StateType.no_data
    else maxState
  }


}
