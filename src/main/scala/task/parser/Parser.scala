package task.parser

import task.model.{Event, Graph}

import scala.io.Source

/**
  * Parser for parsing input json files: graph and events.
  */
trait Parser {


  /**
    * Parses JSON file with given components graph for JSON structure given in task.
    * If structure is not as defined ParserException will be thrown.
    *
    * @param url URL of JSON file
    * @return Graph with components parsed from JSON file
    */
  def parseGraph(url: String): Graph

  /**
    * Parses JSON file with given events for JSON structure given in task.
    * If structure is not as defined ParserException will be thrown.
    *
    * @param url URL of JSON file
    * @return Event list parsed from JSON file
    */
  def parseEvents(url: String): List[Event]
}
