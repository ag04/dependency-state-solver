package task.parser

import task.exception.ParsingException
import task.model.{Component, Event, Graph, StateType}

import scala.io.Source
import scala.util.parsing.json.JSON

/**
  * Parser for parsing input json files: graph and events.
  */
class ParserImpl extends Parser {

  /**
    * Option wrapper class for json properties
    *
    * @tparam T wrapped type
    */
  class CC[T] {
    def unapply(a: Any): Option[T] = Some(a.asInstanceOf[T])
  }

  /**
    * Wrapper for Map[String, Any]
    */
  object M extends CC[Map[String, Any]]

  /**
    * Wrapper for Map[String, String]
    */
  object MS extends CC[Map[String, String]]

  /**
    * Wrapper for List[Any]
    */
  object L extends CC[List[Any]]

  /**
    * Wrapper for List[String]
    */
  object LS extends CC[List[String]]

  /**
    * Wrapper for String
    */
  object S extends CC[String]

  def parseGraph(url: String): Graph = {

    val file = Source.fromFile(url)
    val initialJson = file.getLines mkString "\n" stripMargin

    val result = for {
      Some(M(map)) <- parseFullJsonOrThrow(initialJson)
      M(graph) = getPropertyOrThrow(map, "graph", "root JSON")
      L(components) = getListOrThrow(getPropertyOrThrow(graph, "components", "graph"), "components")
      M(component) <- components
      S(id) = getPropertyOrThrow(component, "id", "component")
      S(ownState) = getPropertyOrThrow(component, "own_state", "component")
      S(derivedState) = getPropertyOrThrow(component, "derived_state", "component")
      MS(checkStates) = component.getOrElse("check_states", Map())
      LS(dependsOn) = component.getOrElse("depends_on", List())
      LS(dependencyOf) = component.getOrElse("dependency_of", List())
    } yield {
      (id, ownState, derivedState, checkStates, dependsOn, dependencyOf)
    }

    val components = result.map(x => new Component(
      x._1,
      StateType.withName(x._2),
      StateType.withName(x._3),
      x._4.map(b => (b._1, StateType.withName(b._2))),
      x._5,
      x._6
    ))

    new Graph(components)
  }

  def parseEvents(url: String): List[Event] = {

    val file = Source.fromFile(url)
    val initialJson = file.getLines mkString "\n" stripMargin

    val events = for {
      Some(M(map)) <- parseFullJsonOrThrow(initialJson)
      L(events) =  getListOrThrow(getPropertyOrThrow(map, "events", "root JSON"),  "events")
      M(event) <- events
      S(timestamp) = getPropertyOrThrow(event, "timestamp", "event")
      S(component) = getPropertyOrThrow(event, "component", "event")
      S(checkState) = getPropertyOrThrow(event, "check_state", "event")
      S(state) = getPropertyOrThrow(event, "state", "event")
    } yield {
      (timestamp, component, checkState, state)
    }

    events.map(x => new Event(
      x._1.toLong,
      x._2,
      x._3,
      StateType.withName(x._4)
    ))
  }

  private def getListOrThrow(value: Any, property: String) = {
    if (value.isInstanceOf[List[Any]]) value
    else throw ParsingException(s"$property property must be list, not object or null!")
  }

  private def parseFullJsonOrThrow(json: String) = {
    val list = List(JSON.parseFull(json).orElse(throw ParsingException("Input file should be JSON object!")))
    if(list.flatten.contains(Nil)) throw ParsingException("Input file should be JSON object!")
    else list
  }

  private def getPropertyOrThrow(map: Map[String, Any], property: String, obj: String) = {
    map.getOrElse(property, throw ParsingException(s"$property property must be defined in $obj!"))
  }
}
