package task.model

import task.util.JsonUtils._
import StateType.StateType

/**
  * Models component as defined in task.
  *
  * @param id                component id
  * @param ownState          initial own state value
  * @param derivedState      initial derived state value
  * @param checkStates       initial check states values
  * @param dependsOnNames    ids of components on which this component depends
  * @param dependencyOfNames ids of components which depends on this component
  */
class Component(var id: String,
                var ownState: StateType,
                var derivedState: StateType,
                var checkStates: Map[String, StateType],
                var dependsOnNames: List[String],
                var dependencyOfNames: List[String]) {

  var dependsOn: List[Component] = List()
  var dependencyOf: List[Component] = List()

  //non functional way of initializing dependency references
  def initDependencies(components: List[Component]): Unit = {
    dependsOn = components.filter(x => dependsOnNames.contains(x.id))
    dependencyOf = components.filter(x => dependencyOfNames.contains(x.id))
  }

  override def equals(obj: scala.Any): Boolean =
    obj match {
    case obj: Component => obj.id == id
    case _ => false
  }

  def getDependsOn: List[Component] = dependsOn

  def getDependencyOf: List[Component] = dependencyOf

  override def toString: String = s"(id: $id, ownState: $ownState, derivedState: $derivedState, checkStates: $checkStates, dependsOn: $dependsOnNames, dependencyOf: $dependencyOfNames)"

  def toJsonString(ind: Int): String = {
    val checkStatesString = checkStates.map(a => indent(4 + ind, toJsonProperty(a._1) + toJsonValue(a._2.toString)))
      .mkString(jsonSeparator + newLine)

    val dependsOnConcat = dependsOn.map(a => indent(4 + ind, toJsonValue(a.id)))
      .mkString(jsonSeparator + newLine)
    val dependsOnString = indent(2 + ind, toJsonProperty("depends_on")) +
      jsonListStart + dependsOnConcat + newLine + indent(2 + ind, jsonListEnd)

    val dependencyOfConcat = dependencyOf.map(a => indent(4 + ind, toJsonValue(a.id)))
      .mkString(jsonSeparator + newLine)
    val dependencyOfString = indent(2 + ind, toJsonProperty("dependency_of")) +
      jsonListStart + dependencyOfConcat + newLine + indent(2 + ind, jsonListEnd)

    val hasOn = dependsOn.nonEmpty
    val hasOf = dependencyOf.nonEmpty
    val afterCheckStatesString = if (hasOn || hasOf) jsonSeparator else emptyString
    val after = if (hasOn && hasOf) dependsOnString + jsonSeparator + newLine + dependencyOfString + newLine
    else if (hasOn) dependsOnString + newLine
    else if (hasOf) dependencyOfString + newLine
    else emptyString

    indent(ind, jsonStart) +
      indent(2 + ind, toJsonProperty("id")) + toJsonValue(id) + jsonSeparator + newLine +
      indent(2 + ind, toJsonProperty("own_state")) + toJsonValue(ownState.toString) + jsonSeparator + newLine +
      indent(2 + ind, toJsonProperty("derived_state")) + toJsonValue(derivedState.toString) + jsonSeparator + newLine +
      indent(2 + ind, toJsonProperty("check_states")) + jsonStart + checkStatesString + newLine +
      indent(2 + ind, jsonEnd) + afterCheckStatesString + newLine + after +
      indent(ind, jsonEnd)
  }
}
