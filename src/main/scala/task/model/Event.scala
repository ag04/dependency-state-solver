package task.model

import StateType.StateType

/**
  * Models event that is fired on Component, as defined in task.
  * @param timestamp defines order of triggering events
  * @param componentId component id
  * @param checkState affected check state of component
  * @param stateType new value of check state
  */
class Event(var timestamp: Long,
            var componentId: String,
            var checkState: String,
            var stateType: StateType) {

  override def toString: String = s"(timestamp: $timestamp, componentId: $componentId, stateType: $stateType)"
}
