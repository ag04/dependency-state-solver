package task.model

/**
  * StateTypes, ordered by priority, from lowest to highest (important for calculating own and derived states).
  */
object StateType extends Enumeration {
  type StateType = Value
  val no_data, clear, warning, alert = Value
}
