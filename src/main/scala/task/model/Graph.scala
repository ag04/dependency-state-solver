package task.model

import task.util.JsonUtils._

/**
  * Models Graph with its components as described in task
  * and provides functionality of firing events on graphs components.
  *
  * @param components graphs components
  */
class Graph(var components: List[Component]) {

  //init graph dependencies
  components.foreach(_.initDependencies(components))

  override def toString: String = components.mkString("\n")

  def toJsonString(ind: Int): String = {
    indent(ind, jsonStart) +
      indent(2 + ind, toJsonProperty("graph")) +
      jsonStart +
      indent(4 + ind, toJsonProperty("components")) +
      jsonListStart +
      components.map(_.toJsonString(6 + ind)).mkString(jsonSeparator + newLine) + newLine +
      indent(4 + ind, jsonListEnd) + newLine +
      indent(2 + ind, jsonEnd) + newLine +
      indent(ind, jsonEnd)
  }

}
