package task.util

/**
  * Utils for converting objects to JSON string.
  */
object JsonUtils {

  def toJsonProperty(s: String): String = toJsonValue(s) + ": "

  def toJsonValue(s: String): String = s"${'"'}$s${'"'}"

  def indent(n: Int, s: String): String = (" " * n) + s

  def jsonSeparator = ","

  def emptyString = ""

  def jsonStart = "{\n"

  def jsonListStart = "[\n"

  def jsonEnd = "}"

  def jsonListEnd = "]"

  def newLine = "\n"

}
