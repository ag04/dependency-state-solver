package task.exception

final case class ParsingException(private val message: String = "",
                       private val cause: Throwable = None.orNull)
  extends Exception(message, cause)