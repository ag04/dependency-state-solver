package task.exception

final case class ServiceViolationException (private val message: String = "",
                                 private val cause: Throwable = None.orNull)
  extends Exception(message, cause)
