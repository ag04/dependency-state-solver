package task.model

import org.scalatest.FunSuite
import stubfactory.EventStubFactory

class EventTest extends FunSuite {

  test("toString") {
    val event = EventStubFactory.createEvent(1)

    assert(event.toString == s"(timestamp: ${event.timestamp}, componentId: ${event.componentId}, stateType: ${event.stateType})")
  }
}
