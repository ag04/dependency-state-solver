package task.model

import org.scalatest.FunSuite
import stubfactory.ComponentStubFactory

import scala.io.Source

class ComponentTest extends FunSuite {

  test("toString") {
    val component = ComponentStubFactory.createComponent(2)

    assert(component.toString == s"(id: ${component.id}," +
      s" ownState: ${component.ownState}, derivedState: ${component.derivedState}," +
      s" checkStates: ${component.checkStates}, dependsOn: ${component.dependsOnNames}," +
      s" dependencyOf: ${component.dependencyOfNames})")
  }

  test("toJsonString"){
    val component = ComponentStubFactory.createComponent(2)
    val file = Source.fromURL(getClass.getResource("/component.json"))
    val json = file.getLines mkString "\n"

    assert(component.toJsonString(3) == json)
  }
}
