package task.model

import org.scalatest.FunSuite
import stubfactory.GraphStubFactory

import scala.io.Source

class GraphTest extends FunSuite {

  test("toString") {
    val graph = GraphStubFactory.createGraph()

    assert(graph.toString == graph.components.mkString("\n"))
  }

  test("toJsonString") {
    val graph = GraphStubFactory.createGraph()
    val file = Source.fromURL(getClass.getResource("/graph.json"))
    val json = file.getLines mkString "\n"

    assert(graph.toJsonString(0) == json)
  }
}
