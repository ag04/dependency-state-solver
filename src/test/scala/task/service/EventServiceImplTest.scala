package task.service

import org.scalatest.FunSuite
import stubfactory.EventStubFactory
import task.exception.ServiceViolationException
import task.model.StateType
import task.parser.ParserImpl

class EventServiceImplTest extends FunSuite{

  var eventService = new EventServiceImpl
  var parser = new ParserImpl

  test("sortEventsByTimestamps"){
    val events = List(
      EventStubFactory.createEvent(5),
      EventStubFactory.createEvent(15),
      EventStubFactory.createEvent(3),
      EventStubFactory.createEvent(1),
      EventStubFactory.createEvent(8)
    )

    val sortedEvents = eventService.sortEventsByTimestamps(events)

    assert(sortedEvents.map(_.timestamp) == List(1,3,5,8,15))
  }

  test("applyEventOnGraph case 1: nothing depends on changed component"){
    val graphUrl = getClass.getResource("/big-graph.json").getPath
    val eventUrl = getClass.getResource("/events/event1.json").getPath

    val graph = parser.parseGraph(graphUrl)
    val event = parser.parseEvents(eventUrl).head

    val result = eventService.applyEventOnGraph(event, graph)

    assert(result.components.head.ownState == StateType.warning)
    assert(result.components.head.derivedState == StateType.warning)
    assert(result.components.head.checkStates("cs1_1") == StateType.warning)
    assert(result.components(1).derivedState == StateType.no_data)
    assert(result.components(2).derivedState == StateType.no_data)
    assert(result.components(3).derivedState == StateType.no_data)
    assert(result.components(4).derivedState == StateType.no_data)
  }

  test("applyEventOnGraph case 2: two components depend on changed one"){
    val graphUrl = getClass.getResource("/big-graph.json").getPath
    val eventUrl = getClass.getResource("/events/event2.json").getPath

    val graph = parser.parseGraph(graphUrl)
    val event = parser.parseEvents(eventUrl).head

    val result = eventService.applyEventOnGraph(event, graph)

    assert(result.components(1).ownState == StateType.warning)
    assert(result.components(1).derivedState == StateType.warning)
    assert(result.components(1).checkStates("cs1_2") == StateType.warning)
    assert(result.components.head.derivedState == StateType.warning)
    assert(result.components(2).derivedState == StateType.warning)
    assert(result.components(3).derivedState == StateType.no_data)
    assert(result.components(4).derivedState == StateType.no_data)
    assert(result.components(5).derivedState == StateType.warning)
    assert(result.components(6).derivedState == StateType.warning)
  }

  test("applyEventOnGraph case 3: one component depends on changed one"){
    val graphUrl = getClass.getResource("/big-graph.json").getPath
    val eventUrl = getClass.getResource("/events/event3.json").getPath

    val graph = parser.parseGraph(graphUrl)
    val event = parser.parseEvents(eventUrl).head

    val result = eventService.applyEventOnGraph(event, graph)

    assert(result.components(2).ownState == StateType.alert)
    assert(result.components(2).derivedState == StateType.alert)
    assert(result.components(2).checkStates("cs1_3") == StateType.alert)
    assert(result.components.head.derivedState == StateType.alert)
    assert(result.components(1).derivedState == StateType.no_data)
    assert(result.components(3).derivedState == StateType.no_data)
    assert(result.components(4).derivedState == StateType.no_data)
  }

  test("applyEventOnGraph case 4: one component depends on changed one bidirectionally"){
    val graphUrl = getClass.getResource("/big-graph.json").getPath
    val eventUrl = getClass.getResource("/events/event4.json").getPath

    val graph = parser.parseGraph(graphUrl)
    val event = parser.parseEvents(eventUrl).head

    val result = eventService.applyEventOnGraph(event, graph)

    assert(result.components(3).ownState == StateType.alert)
    assert(result.components(3).derivedState == StateType.alert)
    assert(result.components(3).checkStates("cs1_4") == StateType.alert)
    assert(result.components.head.derivedState == StateType.no_data)
    assert(result.components(1).derivedState == StateType.no_data)
    assert(result.components(2).derivedState == StateType.no_data)
    assert(result.components(4).derivedState == StateType.alert)
  }

  test("applyEventOnGraph case 5: one component depends on changed one bidirectionally - other direction"){
    val graphUrl = getClass.getResource("/big-graph.json").getPath
    val eventUrl = getClass.getResource("/events/event5.json").getPath

    val graph = parser.parseGraph(graphUrl)
    val event = parser.parseEvents(eventUrl).head

    val result = eventService.applyEventOnGraph(event, graph)

    assert(result.components(4).ownState == StateType.alert)
    assert(result.components(4).derivedState == StateType.alert)
    assert(result.components(4).checkStates("cs1_5") == StateType.alert)
    assert(result.components.head.derivedState == StateType.no_data)
    assert(result.components(1).derivedState == StateType.no_data)
    assert(result.components(2).derivedState == StateType.no_data)
    assert(result.components(3).derivedState == StateType.alert)
  }

  test("applyEventOnGraph when no such component"){
    val graphUrl = getClass.getResource("/big-graph.json").getPath
    val eventUrl = getClass.getResource("/events/event-err1.json").getPath

    val graph = parser.parseGraph(graphUrl)
    val event = parser.parseEvents(eventUrl).head

    val thrown = intercept[ServiceViolationException]{
      eventService.applyEventOnGraph(event, graph)
    }

    assert(thrown.getMessage == "Unknown component in event list: xx")
  }

  test("applyEventOnGraph when no such check state component"){
    val graphUrl = getClass.getResource("/big-graph.json").getPath
    val eventUrl = getClass.getResource("/events/event-err2.json").getPath

    val graph = parser.parseGraph(graphUrl)
    val event = parser.parseEvents(eventUrl).head

    val thrown = intercept[ServiceViolationException]{
      eventService.applyEventOnGraph(event, graph)
    }

    assert(thrown.getMessage == "Unknown check state: xx, for component: 1")
  }

}
