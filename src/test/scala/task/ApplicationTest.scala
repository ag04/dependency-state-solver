package task

import java.util.NoSuchElementException

import org.scalamock.scalatest.MockFactory
import org.scalatest.FunSuite
import stubfactory.EventStubFactory
import task.exception.{ParsingException, ServiceViolationException}
import task.model.Graph
import task.parser.Parser
import task.service.EventService

class ApplicationTest extends FunSuite with MockFactory {

  private val eventService = stub[EventService]
  private val parser = stub[Parser]

  Application.eventService = eventService
  Application.parser = parser

  test("main when successful") {
    val graph = new Graph(List())
    val event = EventStubFactory.createEvent(1)
    val events = List(event, event)
    val arg0 = "arg0"
    val arg1 = "arg1"

    parser.parseGraph _ when arg0 returns graph once()
    parser.parseEvents _ when arg1 returns events once()

    eventService.sortEventsByTimestamps _ when events returns events once()
    eventService.applyEventOnGraph _ when(event, graph) returns graph twice()

    Application.main(Array(arg0, arg1))
  }

  test("main when ParsingException") {
    val arg0 = "arg0"
    val arg1 = "arg1"

    parser.parseGraph _ when arg0 throws ParsingException()

    Application.main(Array(arg0, arg1))
  }

  test("main when ServiceViolationException") {
    val arg0 = "arg0"
    val arg1 = "arg1"

    parser.parseGraph _ when arg0 throws ServiceViolationException()

    Application.main(Array(arg0, arg1))
  }

  test("main when NoSuchElementException") {
    val arg0 = "arg0"
    val arg1 = "arg1"

    parser.parseGraph _ when arg0 throws new NoSuchElementException

    Application.main(Array(arg0, arg1))
  }
}
