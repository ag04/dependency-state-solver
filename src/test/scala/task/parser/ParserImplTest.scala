package task.parser

import org.scalatest.FunSuite
import task.exception.ParsingException
import task.model.StateType

class ParserImplTest extends FunSuite {
  var parser = new ParserImpl

  test("parseGraph when successful") {
    val url = getClass.getResource("/graph1.json").getPath

    val graph = parser.parseGraph(url)

    assert(graph.components.length == 3)

    val component1 = graph.components.head
    val component2 = graph.components(1)
    val component3 = graph.components(2)

    assert(component1.id == "1")
    assert(component1.ownState == StateType.clear)
    assert(component1.derivedState == StateType.no_data)
    assert(component1.checkStates == Map(
      ("cs1_1", StateType.clear), ("cs2_1", StateType.no_data)))
    assert(component1.dependsOn == List(component2, component3))
    assert(component1.dependencyOf == List())

    assert(component2.id == "2")
    assert(component2.ownState == StateType.warning)
    assert(component2.derivedState == StateType.warning)
    assert(component2.checkStates == Map(
      ("cs1_2", StateType.warning), ("cs2_2", StateType.no_data)))
    assert(component2.dependsOn == List())
    assert(component2.dependencyOf == List(component1, component3))

    assert(component3.id == "3")
    assert(component3.ownState == StateType.alert)
    assert(component3.derivedState == StateType.alert)
    assert(component3.checkStates == Map(
      ("cs1_3", StateType.clear), ("cs2_3", StateType.alert)))
    assert(component3.dependsOn == List(component2))
    assert(component3.dependencyOf == List(component1))
  }


  test("parseGraph when parsing non json file") {
    val url = getClass.getResource("/notjsonfile").getPath

    val thrown = intercept[ParsingException]{
      parser.parseGraph(url)
    }

    assert(thrown.getMessage == "Input file should be JSON object!")
  }


  test("parseGraph when parsing non json object") {
    val url = getClass.getResource("/empty-list.json").getPath

    val thrown = intercept[ParsingException]{
      parser.parseGraph(url)
    }

    assert(thrown.getMessage == "Input file should be JSON object!")
  }


  test("parseGraph when missing graph property") {
    val url = getClass.getResource("/graph-err1.json").getPath

    val thrown = intercept[ParsingException]{
      parser.parseGraph(url)
    }

    assert(thrown.getMessage == "graph property must be defined in root JSON!")
  }


  test("parseGraph when missing components property") {
    val url = getClass.getResource("/graph-err2.json").getPath

    val thrown = intercept[ParsingException]{
      parser.parseGraph(url)
    }

    assert(thrown.getMessage == "components property must be defined in graph!")
  }


  test("parseGraph when missing id property") {
    val url = getClass.getResource("/graph-err3.json").getPath

    val thrown = intercept[ParsingException]{
      parser.parseGraph(url)
    }

    assert(thrown.getMessage == "id property must be defined in component!")
  }


  test("parseEvent when missing timestamp property") {
    val url = getClass.getResource("/events-err1.json").getPath

    val thrown = intercept[ParsingException]{
      parser.parseEvents(url)
    }

    assert(thrown.getMessage == "timestamp property must be defined in event!")
  }


  test("parseEvent when missing events property") {
    val url = getClass.getResource("/graph-err1.json").getPath

    val thrown = intercept[ParsingException]{
      parser.parseEvents(url)
    }

    assert(thrown.getMessage == "events property must be defined in root JSON!")
  }


  test("parseEvent when parsing non json file") {
    val url = getClass.getResource("/notjsonfile").getPath

    val thrown = intercept[ParsingException]{
      parser.parseEvents(url)
    }

    assert(thrown.getMessage == "Input file should be JSON object!")
  }

}
