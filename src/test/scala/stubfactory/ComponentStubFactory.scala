package stubfactory

import task.model.{Component, StateType}

object ComponentStubFactory {

  def createComponent(id: Int, dependsOn: List[String] = List("dependsOn"), dependencyOf: List[String] = List("dependencyOf")): Component = new Component(
    id.toString, StateType.clear, StateType.no_data,
    Map(("cs1_" + id, StateType.clear), ("cs2_" + id, StateType.no_data)),
    dependsOn, dependencyOf
  )

}
