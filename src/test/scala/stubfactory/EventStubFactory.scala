package stubfactory

import task.model.{Event, StateType}

object EventStubFactory {

  def createEvent(id: Int): Event = new Event(
    id, "compId" + id, "cS" + id, StateType.no_data
  )

}
