package stubfactory

import task.model.Graph

object GraphStubFactory {

  def createGraph() : Graph = new Graph(List(
    ComponentStubFactory.createComponent(1,List("2","3")),
    ComponentStubFactory.createComponent(2,List(),List("1", "3")),
    ComponentStubFactory.createComponent(3,List("2"),List("1"))
  ))
}
